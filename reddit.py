import praw
import json
import time
import strings
import settings
import datetime
import twitter
from praw import exceptions

reddit = praw.Reddit(user_agent = settings.REDDIT_USER_AGENT,
                        client_id = settings.REDDIT_CLIENT_ID,
                        client_secret = settings.REDDIT_CLIENT_SECRET,
                        username = settings.REDDIT_USERNAME,
                        password = settings.REDDIT_PASSWORD)
def send(title, url):
    #Sends the link and title to the given subreddit as a post
    import html
    while True:
        try:
            post_id = reddit.subreddit(settings.REDDIT_SUBREDDIT).submit(title="{} {}".format(settings.REDDIT_TITLE_TAG,html.unescape(title)), url=url, send_replies=False)
            print(strings.REDDIT_SUBMISSION_SUCCESSFUL.format(url))
        except exceptions.APIException as e:
            print(strings.GENERIC_GOT_ERROR.format(e))
            print(strings.REDDIT_ERROR_RETRY)
            time.sleep(60)
            continue
        break
    return post_id

def handle_json(data):
    try:
        # If retweeted_status is there, its a retweet, so ignore it.
        something = data["retweeted_status"]
        return False
    except KeyError:
        # Exception is raised because it tried to get a key which doesn't exist, this means it's an original post
        name = data["user"]['screen_name']
        title = data['text']
        print(name, title)
        if (name == settings.TWITTER_ACCOUNT_TRACKER_NAME) and "Org.XIII" not in title:
            # Create the twitter link and post it
            url = strings.TWITTER_LINK_URL_UNFORMATTED.format(name, data["id_str"])

            with open("testtweet.txt", "w") as file:
                file.write(str(data))
                file.close()

            submitted_tweet = send(title, url)
            # Find our submitted post as well as make sure the message exists
            if settings.SELF_REPLY_MESSAGE is not "" or settings.SELF_REPLY_MESSAGE is not None:
                replyto = reddit.submission(id=submitted_tweet)
                replyto.reply(body=settings.SELF_REPLY_MESSAGE.format(title, name))
                # Feedback message
                print(strings.REDDIT_POST_SUCCESSFUL.format(datetime.datetime.now().strftime("%d-%B-%Y %I:%M%p")))
        else:
            return False
        return True

if __name__ == "__main__":
    api = twitter.Api(consumer_key=settings.TWITTER_CONSUMER_TOKEN,
                    consumer_secret=settings.TWITTER_CONSUMER_SECRET,
                    access_token_key=settings.TWITTER_ACCESS_TOKEN,
                    access_token_secret=settings.TWITTER_ACCESS_SECRET)
    LANGUAGES = ["en"]
    print(strings.TWITTER_OAUTH_CONNECTION_SUCCESSFUL)
    while True:
        for line in api.GetStreamFilter(track=settings.TWITTER_TRACKER,languages=LANGUAGES):
            data = json.dumps(line)
            data = json.loads(data)
            handle_json(data)
