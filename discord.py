import json
import datetime

import twitter
import requests

import settings
import strings

def send(title, url):
    data = {"content":" ".join(settings.DISCORD_USER_NOFICATION_LIST)+ " " + url + "\n" + title}
    requests.post(settings.DISCORD_WEBHOOK, data=data)


def handle_json(data):
    try:
        # If retweeted_status is there, its a retweet, so ignore it.
        something = data["retweeted_status"]
        return False
    except KeyError:
        # Exception is raised because it tried to get a key which doesn't exist, this means it's an original post
        name = data["user"]['screen_name']
        title = data['text']
        print(name, title)
        if name == settings.TWITTER_ACCOUNT_TRACKER_NAME:
            # Create the twitter link and post it
            url = strings.TWITTER_LINK_URL_UNFORMATTED.format(name, data["id_str"])

            with open("testtweet.txt", "w") as file:
                file.write(str(data))
                file.close()

            send(title, url)
        else:
            return False
        return True

if __name__ == "__main__":
    api = twitter.Api(consumer_key=settings.TWITTER_CONSUMER_TOKEN,
                      consumer_secret=settings.TWITTER_CONSUMER_SECRET,
                      access_token_key=settings.TWITTER_ACCESS_TOKEN,
                      access_token_secret=settings.TWITTER_ACCESS_SECRET)
    LANGUAGES = ["en"]
    while True:
        for line in api.GetStreamFilter(track=settings.TWITTER_TRACKER, languages=LANGUAGES):
            data = json.dumps(line)
            data = json.loads(data)
            handle_json(data)
