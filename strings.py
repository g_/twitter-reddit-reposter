TWITTER_OAUTH_CONNECTION_SUCCESSFUL = "Twitter OAuth connection successful. Now listening to tweets..."
TWITTER_LINK_URL_UNFORMATTED = "https://twitter.com/{}/status/{}"

REDDIT_POST_SUCCESSFUL = "Successfully sent posts on {}, back to listening...\n"
REDDIT_ERROR_RETRY = "Waiting 60 seconds before trying again...\n"
REDDIT_SUBMISSION_SUCCESSFUL = "Successfully submitted: {}"

GENERIC_GOT_ERROR = "Got error: {}"