import re
import time

import praw

import settings

response = "(^ ω ^✿)"

reddit = praw.Reddit(user_agent = settings.REDDIT_USER_AGENT,
						client_id = settings.REDDIT_CLIENT_ID,
						client_secret = settings.REDDIT_CLIENT_SECRET,
						username = settings.REDDIT_USERNAME,
						password = settings.REDDIT_PASSWORD)
while True:
	try:
		for item in reddit.inbox.unread(limit=None):
			if "good bot" in re.sub(r"\W", "", item.body.lower()):
				item.reply(response)
	except Exception as e:
		print(e)
		time.sleep(60)